package br.com.aula.dsd.gson;

import lombok.Getter;

@Getter
public class User {

    private String name;
    private Integer age;
    private Double salary;

    public User(String name, Integer age, Double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}

package br.com.aula.dsd.gson;
import com.google.gson.Gson;

public class App {

    public static void main(String[] args) {

        // Serialização - Java -> JSON

        User user = new User("Samuel", 20, 1000.00);

        String json = "";
        Gson gson = new Gson();

        json = gson.toJson(user);
        System.out.println(json);

        // Desserialização - JSON -> Java

        User fromJson = gson.fromJson(json, User.class);

        System.out.println(fromJson.getName());
        System.out.println(fromJson.getAge());
        System.out.println(fromJson.getSalary());

    }

}
